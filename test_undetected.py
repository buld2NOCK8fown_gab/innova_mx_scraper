import undetected_chromedriver as uc
from selenium import webdriver
from bs4 import BeautifulSoup

options = webdriver.ChromeOptions() 
options.add_argument("start-maximized")
driver = uc.Chrome(options=options)
driver.get('https://www.innovasport.com//tenis/futbol/hombre/c/100010002120000000/update?q=%3Arelevance&page=2')
cat_soup = BeautifulSoup(driver.page_source, "html.parser")
page_results = cat_soup.find_all("div", {"class": "is-pw__product"})
for result in page_results:
    product_url = result.find("a", {"class":"js-gtm-product-click"})
    print(product_url["href"])
    product_dev = result.find("span", {"class":"is-gridwallCard__item__name"})
    print(product_dev.text.strip())