"""
Author: Alain Moré Maceda
Scraper de productos InnovaSport MX
"""
import csv
import json
import logging
import re
import time
import warnings
from datetime import datetime

import jinja2
import pandas as pd
import undetected_chromedriver as uc
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from snowflake.sqlalchemy import URL
from sqlalchemy import create_engine

warnings.filterwarnings("ignore")
logging.basicConfig(
    level=logging.INFO,
    filename="log_scraper.log",
    filemode="a",
    format="%(asctime)s :: %(levelname)s :: %(message)s",
)
log = logging.getLogger("__name__")

# chrome_opt = webdriver.ChromeOptions()
# chrome_opt.add_experimental_option("excludeSwitches", ["enable-automation"])
# chrome_opt.add_experimental_option("useAutomationExtension", False)
# chrome_opt.add_argument(
#     "user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:63.0) Gecko/20100101 Firefox/63.0"
# )
# driver = webdriver.Chrome(chrome_options=chrome_opt)
# driver.maximize_window()
options = webdriver.ChromeOptions()
options.add_argument("start-maximized")
options.page_load_strategy="eager"
driver = uc.Chrome(options=options)

wait = WebDriverWait(driver, 10)
actions = ActionChains(driver)

store_url_root = "https://www.innovasport.com"
added_models = []


def set_query_tag(connection):
    select = """
        ALTER SESSION SET QUERY_TAG = "global-ecommerce-data";
    """
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(select)
    formatted_template = template.render()
    log.info(formatted_template)
    try:
        df_query_tag = pd.read_sql_query(con=connection, sql=formatted_template)
        log.info(df_query_tag)
    except Exception as ex:
        log.error(ex)


def cleanup_snowflake_tables(connection):
    continue_process = True
    log.info("Cloning backup data")
    clone = "CREATE OR REPLACE TABLE GLOBAL_ECOMMERCE.INNOVA_PRODUCTS_BCK CLONE GLOBAL_ECOMMERCE.INNOVA_PRODUCTS"
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(clone)
    formatted_template = template.render()
    log.info(formatted_template)
    try:
        df_clone = pd.read_sql_query(con=connection, sql=formatted_template)
        log.info("Clone OK")
        log.info(df_clone)
    except Exception as ex:
        continue_process = False
        log.exception("Clone ERROR")

    if continue_process:
        log.info("Deleting old data")
        delete = "DELETE FROM GLOBAL_ECOMMERCE.INNOVA_PRODUCTS"
        template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(delete)
        formatted_template = template.render()
        log.info(formatted_template)
        try:
            df_delete = pd.read_sql_query(con=connection, sql=formatted_template)
            log.info("Delete OK")
            log.info(df_delete)
        except Exception as ex:
            continue_process = False
            log.Exception("Delete ERROR")

    return continue_process


def insert_product(connection, product):
    """
    Inserts current processed product into Snowflake
    """
    insert = """
        INSERT INTO GLOBAL_ECOMMERCE.INNOVA_PRODUCTS 
            (MODEL,NAME,CATEGORY,CURRENT_PRICE,PREVIOUS_PRICE,AVG_RATING,TOTAL_RATINGS,PROMO_TAG,
                PICTURES,SIZES,DESCRIPTION,URL,SCRAPE_DATETIME)
        VALUES(
            '{{product_model}}',
            '{{product_name}}',
            '{{product_category}}',
            {{product_current_price}},
            {{product_previous_price}},
            {{product_avg_rating}},
            {{product_total_ratings}},
            '{{product_promo}}',
            '{{pictures}}',
            '{{sizes}}',
            '{{product_desc}}',
            '{{product_url}}',
            '{{scrape_datetime}}'  
        )     
    """
    template = jinja2.Environment(loader=jinja2.BaseLoader).from_string(insert)
    formatted_template = template.render(**product)
    log.info(formatted_template)
    try:
        df_insert = pd.read_sql_query(con=connection, sql=formatted_template)
        log.info("Insert OK")
        log.info(df_insert)
    except Exception as ex:
        log.info("Insert ERROR")
        log.error(ex)


def get_dom(url):
    """
    gets the DOM of the requested url using selenium NA
    """
    driver.get(url)


def start_scraping(connection, url):
    """
    Scraping main function, orchestrates full process
    """
    log.info("")
    log.info(">>>>>>>> CATEGORIA: %s", url)
    log.info("")
    get_dom(url)
    page_number = 1
    cat_soup = BeautifulSoup(driver.page_source, "html.parser")
    prod_category = cat_soup.find(
        "h1", {"class": "is-grid-banner__container__title"}
    ).text.strip()
    total_products_tag = cat_soup.find(
        "span", {"class": "is-pw__results js-number-of-results"}
    )
    if total_products_tag:
        total_products = re.findall(r"\d+", total_products_tag.text.strip())[-1]
    if int(total_products) > 0:
        results = cat_soup.find_all("div", {"class": "is-pw__product"})
    log.info("Productos en pagina %i: %i", page_number, len(results))

    has_next_page = True
    time.sleep(2)
    while has_next_page:
        next_page_tag = cat_soup.find("li", {"class", "is-gridwall-pagination__next"})
        last_page_tag = cat_soup.find(
            "li", {"class", "is-gridwall-pagination__next-double"}
        )
        no_next_page_tag = cat_soup.find(
            "li", {"class", "is-gridwall-pagination__next pageMoveDisabled"}
        )
        no_last_page_tag = cat_soup.find(
            "li", {"class", "is-gridwall-pagination__next-double pageMoveDisabled"}
        )
        if (
            next_page_tag
            and last_page_tag
            and not no_next_page_tag
            and not no_last_page_tag
        ):
            has_next_page = True
            log.info("Tiene siguiente pagina!")
            next_page_url = store_url_root + next_page_tag.find("a")["href"]
            log.info(next_page_url)
            get_dom(next_page_url)
            page_number = page_number + 1
            time.sleep(2)
            cat_soup = BeautifulSoup(driver.page_source, "html.parser")
            total_products_tag = cat_soup.find(
                "span", {"class": "is-pw__results js-number-of-results"}
            )
            if total_products_tag:
                total_products = re.findall(r"\d+", total_products_tag.text.strip())[-1]
                if int(total_products) > 0:
                    page_results = cat_soup.find_all("div", {"class": "is-pw__product"})

                    results = results + page_results
                    log.info(
                        "Productos en pagina %i: %i", page_number, len(page_results)
                    )
        else:
            has_next_page = False
            cat_soup = BeautifulSoup(driver.page_source, "html.parser")

    log.info("results iniciales: %s", str(len(results)))
    product_urls = []
    for result in results:
        product_url = (result.find("a", {"class": "js-gtm-product-click"}))["href"]
        product_urls.append(product_url)

    log.info("lista de urls preliminar: , %i", len(product_urls))
    product_urls = list(set(product_urls))
    log.info("lista de urls limpia: , %i", len(product_urls))
    log.info("")
    log.info("")

    # Obtener detalles desde la página del producto
    for product_url in product_urls:
        product = {}
        product_model = ""
        get_dom(store_url_root + product_url)
        time.sleep(1)
        prod_soup = BeautifulSoup(driver.page_source, "html.parser")

        # MODELO
        tmp = prod_soup.find("p", {"id": "productModel"})
        if tmp:
            product_model = (tmp.text.strip()).split(" ")[1]

            if product_model in added_models:
                log.info(
                    ">>>>> EL PRODUCTO %s YA FUE PREVIAMENTE SCRAPEADO!", product_model
                )
            else:
                log.info(">>>>> EL PRODUCTO %s ES NUEVO!", product_model)
                added_models.append(product_model)

                # CHECK FOR POSSIBLE COLOR VARIATIONS AND ADD THEM AS DIFFERENT PRODUCTS
                no_variations_tag = prod_soup.select(
                    'div[class="product-colorways row hide"]'
                )
                if no_variations_tag:
                    log.info(">> SIN variaciones!")
                else:
                    variations_container = prod_soup.find(
                        "div", {"class": "product-colorways__content"}
                    )
                    if variations_container:
                        log.info(">> CON variaciones!")
                        variations_urls = variations_container.find_all("a")
                        for variation_url in variations_urls:
                            if (
                                variation_url["href"] not in product_urls
                                and variation_url["href"] != "#"
                            ):
                                product_urls.append(variation_url["href"])
                                log.info("> variación nueva, agregando a lista...")
                            else:
                                log.info(
                                    "> variación ya existia en lista, ignorando..."
                                )

                # MODEL
                product["product_model"] = product_model

                # NAME
                try:
                    product_name = prod_soup.find(
                        "h1", {"id": "productName"}
                    ).text.strip()
                    product["product_name"] = product_name.replace("'","")
                except Exception as ex:
                    log.info("No se pudo obtener el nombre del producto")
                    log.error(ex)

                # CATEGORY
                product["product_category"] = prod_category

                # PRECIO ACTUAL
                try:
                    product_current_price = (
                        prod_soup.find("span", {"id": "pdpCurrent_wholePart"})
                        .text.strip()
                        .replace(",", "")
                        .replace(".", "")
                    )
                    product["product_current_price"] = float(product_current_price)
                except Exception as ex:
                    log.info("No se pudo obtener el precio actual")
                    log.error(ex)

                # PRECIO ANTERIOR
                try:
                    product_previous_price = (
                        prod_soup.find("span", {"id": "pdpHighest_wholePart"})
                        .text.strip()
                        .replace(",", "")
                        .replace(".", "")
                    )
                    product["product_previous_price"] = float(product_previous_price)
                except Exception as ex:
                    log.info("No se pudo obtener el precio anterior")
                    product["product_previous_price"] = product["product_current_price"]

                # CALIFICACION PROMEDIO
                try:
                    product_avg_rating = prod_soup.find(
                        "div", {"itemprop": "ratingValue"}
                    ).text.strip()
                    product["product_avg_rating"] = float(product_avg_rating)
                except Exception as ex:
                    log.info("No se pudo obtener el rating")
                    product["product_avg_rating"] = 0.0

                # NUMERO DE CALIFICACIONES
                try:
                    product_total_ratings = (
                        prod_soup.find("div", {"class": "bv_numReviews_text"})
                        .text.strip()
                        .replace("(", "")
                        .replace(")", "")
                    )
                    product["product_total_ratings"] = int(product_total_ratings)
                except Exception as ex:
                    log.info("No se pudo obtener la cantidad de calificaciones")
                    product["product_total_ratings"] = 0

                # PROMO TAG
                tmp_product_promo = prod_soup.find("a", {"id": "promoFlagLabel"})
                if tmp_product_promo:
                    product_promo = tmp_product_promo.text.strip()
                    if product_promo == "Overtime":
                        product["product_promo"] = "Outlet"
                    else:
                        product["product_promo"] = product_promo
                else:
                    product["product_promo"] = ""

                # PICTURES
                pictures = []
                picture_container = prod_soup.find("div", {"class": "slick-track"})
                if picture_container:
                    picture_tags = picture_container.find_all("img")
                    if picture_tags:
                        max_pictures = 0
                        for pic_tag in picture_tags:
                            pictures.append(store_url_root + pic_tag["src"])
                            max_pictures = max_pictures + 1
                            if max_pictures >= 3:
                                break

                        pics_json = json.dumps(pictures)
                        product["pictures"] = pics_json
                    else:
                        log.info("No se pudieron obtener imagenes")
                        product["pictures"] = "[]"
                else:
                    log.info(
                        "No se pudieron obtener imagenes - No se encontro container"
                    )
                    product["pictures"] = "[]"

                # SIZES
                try:
                    sizes = []
                    sizes_container = prod_soup.find("div", {"id": "nav-online-size"})
                    if sizes_container:
                        size_tags = sizes_container.find_all(
                            "a",
                            {
                                "class": "product-size__option product-size__option--selected"
                            },
                        )
                        size_tags = size_tags + sizes_container.select(
                            'a[class="product-size__option"]'
                        )
                        for size_tag in size_tags:
                            sizes.append(float(size_tag.text.strip()))
                        sizes_json = json.dumps(sizes)
                        product["sizes"] = sizes_json
                    else:
                        log.info(
                            "No se pudo obtener el container de tallas del producto - if"
                        )
                        log.error(ex)
                        product["product_sizes"] = "[]"
                except:
                    log.info("No se pudo obtener el container de tallas del producto")
                    log.error(ex)
                    product["product_sizes"] = "[]"

                # DESCRIPCION
                try:
                    product_desc = prod_soup.find(
                        "div",
                        {"class": "is-accordion__item-wrapper is-accordion__item-html"},
                    ).text
                    product["product_desc"] = product_desc.replace("'","")
                except Exception as ex:
                    log.info("No se pudo obtener la descripción del producto")
                    log.error(ex)
                    product["product_desc"] = ""

                # URL
                product["product_url"] = store_url_root + product_url

                product["scrape_datetime"] = datetime.now()

                insert_product(connection, product)
                log.info(product)
                log.info("")
                log.info("")
                log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
                log.info("")
                log.info("")

        else:
            log.info("No se pudo obtener el modelo del producto")

        log.info("")


def main():
    """
    Función main, ejecuta el proceso paso por paso
    """
    print("PROCESS START!")
    snowflake_configuration = {
        "url": "hg51401.snowflakecomputing.com",
        "account": "hg51401",
        "user": "ALAIN.MORE@RAPPI.COM",
        "authenticator": "externalbrowser",
        "port": 443,
        "warehouse": "ECOMMERCE",
        "role": "GLOBAL_ECOMMERCE_WRITE_ROLE",
        "database": "fivetran",
    }
    engine = create_engine(URL(**snowflake_configuration))
    connection = engine.connect()
    try:
        set_query_tag(connection)
        continue_process = cleanup_snowflake_tables(connection)
        continue_process = True
        if continue_process:
            with open("categories.csv", "r") as csvfile:
                csvreader = csv.DictReader(csvfile)
                for row in csvreader:
                    cat_url = row["category_url"]
                    start_scraping(connection, cat_url)

            driver.close()
            print("PROCESS FINISHED OK!")
        else:
            print("Error al respaldar y/o borrar data anterior")
    finally:
        connection.close()
        engine.dispose()
        log.info("Engine y connection cerrados")


if __name__ == "__main__":
    main()
